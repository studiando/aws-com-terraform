variable "env" {

}

variable "aws_region" {
  type        = string
  description = "aws region"
  default     = "us-east-1"
}

variable "aws_profile" {
  type        = string
  description = "aws profile"
  default     = "auk-dev"
}

variable "instance_ami" {
  type        = string
  description = "instance ami"
  default     = "ami-0715c1897453cabd1"
  validation {
    condition     = length(var.instance_ami) > 4 && substr(var.instance_ami, 0, 4) == "ami-"
    error_message = "The instance_ami value must be a valid AMI id, starting wiht \"ami-\"."
  }
}

variable "instance_number" {
  type = object({
    dev  = number
    prod = number
  })
  description = "number of instances to create"
  default = {
    dev  = 1
    prod = 2
  }
}

variable "instance_type" {
  type = object({
    dev  = string
    prod = string
  })
  description = "instance type"
  default = {
    dev  = "t3.micro"
    prod = "t3.medium"
  }
}
