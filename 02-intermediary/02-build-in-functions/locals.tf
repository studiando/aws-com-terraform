locals {
  instance_number = lookup(var.instance_number, var.env)
  file_ext        = "zip"
  object_name     = "my-generated-file-from-template"
  common_tags = {
    "Owner" = "Vinicius Henrique"
    "Year"  = "2023"
  }
}
