terraform apply -var="env=dev" -auto-approve
terraform apply -var="env=prod" -auto-approve
terraform destroy -var="env=prod" -auto-approve
