resource "aws_iam_user" "accounts" {
  for_each = toset(["Todd", "James", "Alice", "Dottie"])
  name     = each.key # note: each.key and each.value are the same for a set
}
