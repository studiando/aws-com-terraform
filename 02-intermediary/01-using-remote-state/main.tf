terraform {
  required_version = "1.5.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.1.0"
    }
  }
  backend "s3" {
    bucket  = "tfstate-532634643942"
    key     = "dev/01-using-remote-state/terraform.tfstate"
    region  = "us-east-1"
    profile = "auk-dev"
  }
}

provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile
}
