variable "aws_region" {
  type        = string
  description = ""
  default     = "us-east-1"
}

variable "aws_profile" {
  type        = string
  description = ""
  default     = "auk-dev"
}

variable "ami" {
  type        = string
  description = ""
  default     = "ami-0715c1897453cabd1"
}

variable "instance_type" {
  type        = string
  description = ""
  default     = "t3.micro"
}
