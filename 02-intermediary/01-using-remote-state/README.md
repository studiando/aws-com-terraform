terraform init -backend=true -backend-config="bucket=tfstate-532634643942" -backend-config="key=dev/01-using-remote-state/terraform.tfstate" -backend-config="region=us-east-1" -backend-config="profile=auk"

terraform init -backend=true -backend-config="backend.hcl"

terraform destroy -auto-approve
