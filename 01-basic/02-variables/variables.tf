variable "environment" {
  type        = string
  description = "environment name"
}

variable "aws_region" {
  type        = string
  description = "region"
}

variable "aws_profile" {
  type        = string
  description = "aws profile"
}

variable "instance_ami" {
  type        = string
  description = "ec2 instance ami type"
}

variable "instance_type" {
  type        = string
  description = "ec2 instance type"
}

variable "instance_tags" {
  type        = map(string)
  description = "ec2 instance tags"
  default = {
    Name    = "Amazon Linux"
    Project = "Curso AWS com Terraform"
  }
}
