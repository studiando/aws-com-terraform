terraform apply -var="environment=dev" -auto-approve

terraform plan -var-file="prod.tfvars"
terraform plan -var="environment=prod"
TF_VAR_environment=prod terraform plan
