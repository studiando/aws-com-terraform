environment   = "prod"
instance_type = "t3.medium"
instance_tags = {
  Name    = "Amazon Linux - Prod"
  Project = "Curso AWS com Terraform"
}