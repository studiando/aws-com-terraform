terraform {
  required_version = "1.5.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.1.0"
    }
  }
}

provider "aws" {
  region  = "us-east-1"
  profile = "auk-dev"
}

resource "aws_s3_bucket" "b" {
  bucket = "auk-testing-aws-com-terraform"

  tags = {
    Name        = "auk-testing-aws-com-terraform"
    Environment = var.environment
    ManagedBy   = "Terraform"
  }
}

resource "aws_s3_bucket_ownership_controls" "b" {
  bucket = aws_s3_bucket.b.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

resource "aws_s3_bucket_acl" "b" {
  depends_on = [aws_s3_bucket_ownership_controls.b]

  bucket = aws_s3_bucket.b.id
  acl    = "private"
}
