variable "aws_region" {
  type        = string
  description = "aws region"
  default     = "us-east-1"
}

variable "aws_profile" {
  type        = string
  description = "aws profile"
  default     = "auk-dev"
}

variable "environment" {
  type        = string
  description = "environment name"
  default     = "dev"
}
